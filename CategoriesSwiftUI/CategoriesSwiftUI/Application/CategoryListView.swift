//
//  CategoryListView.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import SwiftUI
import Combine

struct CategoryView: View {
    let category: CategoryModel
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10.0)
                .fill(Color.white)
            VStack {
                if let image = category.image?.url,
                    let url = URL(string: image) {
                    AsyncImage(
                        url: url,
                        placeholder: { Text("Loading ...") },
                        image: { Image(uiImage: $0)
                            .resizable() }
                    )
                }
                Text(category.name)
                    .padding(2)
                    .foregroundColor(Color("Primary"))
                    .font(Font.system(size: 12))
            }
        }.frame(minHeight: 100)
    }
}

struct CategoryListView: View {
    
    @ObservedObject var viewModel: CategoryViewModel
    
    private let columns = [
            GridItem(.flexible(minimum: 90), spacing: 16),
            GridItem(.flexible(minimum: 90), spacing: 16),
        ]
    
    init(viewModel: CategoryViewModel) {
        self.viewModel = viewModel

        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = UIColor(named: "Primary")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 16) {
                    ForEach(viewModel.categories, content: CategoryView.init)
                }
                .padding(.all, 16)
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle(Text("Channels Categories"))
            .background(Color("Background"))
        }
        .onAppear {
            viewModel.getCategories()
        }
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryListView(viewModel: CategoryViewModel())
    }
}
