//
//  CategoryViewModel.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation
import Combine
import SwiftUI

class CategoryViewModel: ObservableObject, CategoriesService {
    var apiSession: APIService
    
    @Published var categories = [CategoryModel]()
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getCategories() {
        let cancellable = self.list().map([CategoryModel].init)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }
                
            }) { categories in
                self.categories = categories
        }
        cancellables.insert(cancellable)
    }
}
