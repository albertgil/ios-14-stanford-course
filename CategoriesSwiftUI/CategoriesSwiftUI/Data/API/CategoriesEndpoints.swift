//
//  CategoriesEndpoints.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation

enum CategoriesEndpoints {
    case list
}

extension CategoriesEndpoints: RequestBuilder {
    
    var urlRequest: URLRequest {
        switch self {
        case .list:
            guard let url = URL(string: "http://devcms.ayoba.me/api/v4/category")
                else {preconditionFailure("Invalid URL format")}
            let request = URLRequest(url: url)
            return request
        }
        
    }
}
