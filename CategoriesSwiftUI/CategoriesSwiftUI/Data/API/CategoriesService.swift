//
//  CategoriesService.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Combine

protocol CategoriesService {
    var apiSession: APIService { get }
    
    func list() -> AnyPublisher<[CategoryDto], APIError>
}

extension CategoriesService {
    
    func list() -> AnyPublisher<[CategoryDto], APIError> {
        return apiSession.request(with: CategoriesEndpoints.list)
            .eraseToAnyPublisher()
    }
}
