//
//  CategoryDto.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation

struct CategoryDto: Codable, Identifiable {
    let id: String
    let uuid: String
    let name: String
    let image: ImageDto?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        uuid = try container.decode(String.self, forKey: .uuid)
        let decodedName = try container.decode(String.self, forKey: .name)
        name = decodedName.htmlDecoded
        image = try? container.decodeIfPresent(ImageDto.self, forKey: .image)
    }
}

struct ImageDto: Codable {
    let url: String
}
