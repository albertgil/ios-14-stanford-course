//
//  APIError.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation

enum APIError: Error {
    case decodingError
    case httpError(Int)
    case unknown
}
