//
//  RequestBuilder.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation

protocol RequestBuilder {
    var urlRequest: URLRequest { get }
}
