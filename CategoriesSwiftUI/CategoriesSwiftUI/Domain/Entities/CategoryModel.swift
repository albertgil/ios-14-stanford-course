//
//  CategoryModel.swift
//  CategoriesSwiftUI
//
//  Created by Albert Gil Escura on 7/3/21.
//

import Foundation

struct CategoryModel: Identifiable {
    let id: String
    let uuid: String
    let name: String
    let image: ImageModel?
}

extension CategoryModel {
    
    init(with dto: CategoryDto) {
        self.id = dto.id
        self.uuid = dto.uuid
        self.name = dto.name
        if let image = dto.image {
            self.image = ImageModel(with: image)
        } else {
            self.image = nil
        }
    }
}

struct ImageModel {
    let url: String
}

extension ImageModel {
    
    init(with dto: ImageDto) {
        self.url = dto.url
    }
}

extension Array where Element == CategoryModel {
    
    init(with dtos: [CategoryDto]) {
        self = dtos.map { CategoryModel(with: $0) }
    }
}
