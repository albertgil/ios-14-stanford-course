//
//  Array+Only.swift
//  Memorize
//
//  Created by Albert Gil Escura on 13/2/21.
//

import Foundation

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
